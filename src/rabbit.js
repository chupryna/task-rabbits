"use strict";

export default class Rabbit {
  constructor(name) {
    this._name = name;
    this._x = 0;
    this._y = 0;
    this._listeners = [];
  }
  get name() {
    return this._name;
  }
  jump() {
    this._x = Math.round(100 * Math.random());
    this._y = Math.round(100 * Math.random());
    console.log(
      `Rabbit ${this._name} jumped at point x: ${this._x} y: ${this._y}`
    );
    this._notify();
  }
  on(func) {
    this._listeners.push(func);
  }
  off(func) {
    this._listeners = this._listeners.filter(item => item !== func);
  }
  _notify() {
    this._listeners.forEach(func => func(this));
  }
}
