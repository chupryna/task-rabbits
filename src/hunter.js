"use strict";

import { markWinner } from "./functions";

export default class Hunter {
  constructor(name, forest) {
    this._name = name;
    this._x = 0;
    this._y = 0;
    this.shoot = this.shoot.bind(this);
    this._forest = forest;
  }
  get name() {
    return this._name;
  }
  shoot(rabbit) {
    shotSound.volume = 0.5;
    shotSound.play();
    this._x = Math.round(100 * Math.random());
    this._y = Math.round(100 * Math.random());
    let distance = (this._x - rabbit._x) ** 2 + (this._y - rabbit._y) ** 2;
    let result = `Hunter ${this._name} failed to kill rabbit ${rabbit._name}`;

    if (distance <= 900) {
      result = `Hunter ${this._name} killed rabbit ${rabbit._name}`;
      this._forest.killRabbit(rabbit);
      markWinner(this._name);
    }
    console.log(
      `Hunter ${this._name} shut at point x: ${this._x} y: ${this._y}`,
      result
    );
  }
}
