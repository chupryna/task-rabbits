"use strict";

import Rabbit from "./rabbit";
import Hunter from "./hunter";
import { delay } from "./functions";
export default class Forest {
  constructor() {
    this._rabbits = [];
    this._hunters = [];
    this._r = 0;
    this._h = 0;
  }
  addRabbit() {
    let name = `r${this._r++}`;
    let rabbit = new Rabbit(name);
    this._rabbits.push(rabbit);
    this._hunters.forEach(hunter => rabbit.on(hunter.shoot));
    return rabbit;
  }
  killRabbit(rabbit) {
    this._rabbits = this._rabbits.filter(r => r._name !== rabbit._name);
    this._hunters.forEach(hunter => rabbit.off(hunter.shoot));
    let r = document.getElementById(rabbit.name);
    r.classList.add("killed");
    delay(1300).then(() => {
      crySound.volume = 1;
      crySound.play();
    });

    delay(2000).then(() => {
      let r = document.getElementById(rabbit.name);
      if (r) forestFild.removeChild(r);
    });
  }

  addHunter() {
    let name = `h${this._h++}`;
    let hunter = new Hunter(name, this);
    this._hunters.push(hunter);
    this._rabbits.forEach(rabbit => rabbit.on(hunter.shoot));
    return hunter;
  }

  removeHunter(hunter) {
    this._hunters = this._hunters.filter(h => h._name !== hunter._name);
    this._rabbits.forEach(rabbit => rabbit.off(hunter.shoot));
  }
}
