"use strict";

export function markWinner(name) {
  let h = document.getElementById(name);
  h.classList.add("winner");
  setTimeout(() => h.classList.remove("winner"), 3000);
}
export function delay(time) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(), time);
  });
}
