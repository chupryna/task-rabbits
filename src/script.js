"use strict";

import "./style.css";

import Forest from "./forest";

let f1 = new Forest();
addRabbitButton.addEventListener("click", () => addNewRabbit());
function addNewRabbit() {
  let rabbit = f1.addRabbit();
  let rabbitImage = document.createElement("div");
  rabbitImage.id = rabbit.name;
  rabbitImage.className = "rabbitImg";
  let i = Number(rabbit.name.substr(1));
  rabbitImage.style.top = `${20 + Math.round(50 * Math.random())}px`;
  rabbitImage.style.left = `${20 + Math.round(50 * Math.random())}px`;
  forestFild.appendChild(rabbitImage);
}

jumpButton.addEventListener("click", () => rabbitJump());
function rabbitJump() {
  f1._rabbits.forEach(rabbit => {
    rabbit.jump();
    let r = document.getElementById(rabbit.name);
    r.style.top = `${25 + 2.5 * rabbit._y}px`;
    r.style.left = `${25 + 2.5 * rabbit._x}px`;
  });
}

addHunterButton.addEventListener("click", () => addNewHunter());
function addNewHunter() {
  let hunter = f1.addHunter();
  let hunterImage = document.createElement("div");
  hunterImage.id = hunter.name;
  hunterImage.className = "hunterImg";
  hunterImage.innerText = hunter.name;
  huntersFild.appendChild(hunterImage);
}

removeHunterButton.addEventListener("click", () => removeOneHunter());
function removeOneHunter() {
  let hunter = f1._hunters.pop();
  let h = document.getElementById(hunter.name);
  huntersFild.removeChild(h);
  f1.removeHunter(hunter);
}
